import React from "react";
import AdicionaFruta from "../AdicionaFruta/AdicionaFruta";
import Fruta from "../Fruta/Fruta";
import { useSelector } from "react-redux";

const ListaFrutas = () => {
  const frutas = useSelector((state) => state.frutaReducers.frutas);
  return (
    <div className="ListaFrutas">
      <h1>Lista de frutas</h1>

      <AdicionaFruta />

      {frutas.map((fruta) => (
        <Fruta key={fruta.id} fruta={fruta}></Fruta>
      ))}
    </div>
  );
};

export default ListaFrutas;
